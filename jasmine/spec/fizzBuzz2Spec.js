describe("fizzBuzz", function() {

var fizzBuzz;

beforeEach(function(){
  fizzBuzz = new FizzBuzz();
});

describe('multiples of 3', function() {
it('fizzes for 3', function() {
  expect(fizzBuzz.play(3)).toEqual('Fizz');
});



}); //describe


describe('multiples of 5', function() {
  it('fizzes for 5', function() {
  expect(fizzBuzz.play(5)).toEqual('Buzz');
});

});

it('fizzes for 4', function(){
  expect(fizzBuzz.play(4)).toEqual(4)
});

});
