describe("fizzBuzz", function() {
  // var result;
  //
  // beforeEach(function(){
  //   result = new fizzBuzz(number);
  // });

  it("prints 1 when passed 1", function() {
    expect(fizzBuzz(1)).toEqual(1);
  });

  it("prints 2 when passed 2", function() {
    expect(fizzBuzz(2)).toEqual(2);
  });

  it("prints 'fizz' when passed 3", function() {
    expect(fizzBuzz(3)).toEqual('fizz');
  });

  it("prints 'buzz' when passed 5", function() {
    expect(fizzBuzz(5)).toEqual('buzz');
  });

  it("prints 'fizz' when passed 6", function() {
    expect(fizzBuzz(6)).toEqual('fizz');
  });

  it("prints 'buzz' when passed 10", function() {
    expect(fizzBuzz(10)).toEqual('buzz');
  });

  it("prints 'fizzbuzz' when passed 15", function() {
    expect(fizzBuzz(15)).toEqual('fizzbuzz');
  });

  it("prints 'fizzbuzz' when passed 30", function(){
    expect(fizzBuzz(30)).toEqual('fizzbuzz');
  })

});
